package com.jartdeveloper.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jartdeveloper.model.Venta;

@Repository
public interface IVentaDAO extends JpaRepository<Venta, Integer>{

}
