package com.jartdeveloper.services;

import java.util.List;

import com.jartdeveloper.model.Venta;

public interface IVentaService {

	Venta guardar(Venta venta);

	void modificar(Venta venta);

	void eliminar(int id);

	List<Venta> listar();

	Venta consultarPorId(int id);

}
