package com.jartdeveloper.services;

import java.util.List;

import com.jartdeveloper.model.Producto;

public interface IProductoService {

	Producto guardar(Producto producto);

	void modificar(Producto producto);

	void eliminar(int id);

	List<Producto> listar();

	Producto consultarPorId(int id);
}
