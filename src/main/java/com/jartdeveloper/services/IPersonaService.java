package com.jartdeveloper.services;

import java.util.List;

import com.jartdeveloper.model.Persona;

public interface IPersonaService {

	Persona guardar(Persona persona);

	void modificar(Persona persona);

	void eliminar(int id);

	List<Persona> listar();

	Persona consultarPorId(int id);

}
