package com.jartdeveloper.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jartdeveloper.dao.IPersonaDAO;
import com.jartdeveloper.model.Persona;
import com.jartdeveloper.services.IPersonaService;

@Service
public class PersonaServiceImpl implements IPersonaService {

	@Autowired
	IPersonaDAO _personaDAO;

	@Override
	public Persona guardar(Persona persona) {
		return _personaDAO.save(persona);
	}

	@Override
	public void modificar(Persona persona) {
		_personaDAO.save(persona);

	}

	@Override
	public void eliminar(int id) {
		_personaDAO.delete(id);

	}

	@Override
	public List<Persona> listar() {
		return _personaDAO.findAll();
	}

	@Override
	public Persona consultarPorId(int id) {
		return _personaDAO.findOne(id);
	}

}
