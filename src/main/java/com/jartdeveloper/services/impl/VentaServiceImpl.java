package com.jartdeveloper.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jartdeveloper.dao.IVentaDAO;
import com.jartdeveloper.model.Venta;
import com.jartdeveloper.services.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService{

	@Autowired
	IVentaDAO _ventaDAO;
	
	@Override
	public Venta guardar(Venta venta) {
		venta.getDetalleVentas().forEach(a -> a.setVenta(venta));
		return _ventaDAO.save(venta);
	}

	@Override
	public void modificar(Venta venta) {
		_ventaDAO.save(venta);
	}

	@Override
	public void eliminar(int id) {
		_ventaDAO.delete(id);
	}

	@Override
	public List<Venta> listar() {
		return _ventaDAO.findAll();
	}

	@Override
	public Venta consultarPorId(int id) {
		return _ventaDAO.findOne(id);
	}

	
	
}
