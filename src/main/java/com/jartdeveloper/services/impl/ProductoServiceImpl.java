package com.jartdeveloper.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jartdeveloper.dao.IProductoDAO;
import com.jartdeveloper.model.Producto;
import com.jartdeveloper.services.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	IProductoDAO _productoDAO;

	@Override
	public Producto guardar(Producto producto) {
		return _productoDAO.save(producto);
	}

	@Override
	public void modificar(Producto producto) {
		_productoDAO.save(producto);
	}

	@Override
	public void eliminar(int id) {
		_productoDAO.delete(id);
	}

	@Override
	public List<Producto> listar() {
		return _productoDAO.findAll();
	}

	@Override
	public Producto consultarPorId(int id) {
		return _productoDAO.findOne(id);
	}

}
