package com.jartdeveloper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullStackTarea1Application {

	public static void main(String[] args) {
		SpringApplication.run(FullStackTarea1Application.class, args);
	}
}
