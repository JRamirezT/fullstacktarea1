package com.jartdeveloper.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jartdeveloper.model.Venta;
import com.jartdeveloper.services.IVentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {

	@Autowired
	private IVentaService _ventaServices;

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> listar() {
		List<Venta> lista = new ArrayList<>();
		lista = _ventaServices.listar();
		return new ResponseEntity<List<Venta>>(lista, HttpStatus.OK);
	}

	@PostMapping(value = "/guardar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Venta> guardar(@RequestBody Venta venta) {
		Venta ven = new Venta();
		try {
			ven = _ventaServices.guardar(venta);
			System.out.println(ven.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Venta>(ven, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Venta>(ven, HttpStatus.OK);
	}
}
